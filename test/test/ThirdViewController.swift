//
//  ThirdViewController.swift
//  test
//
//  Created by Andrey Maksymov on 03.10.2021.
//

import UIKit

class ThirdViewController: UIViewController {
    var delegate: ButtonDelegate?
    var surname = ""
    var name = ""
    
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = "Имя: \(name)"
        surnameLabel.text = "Фамилия: \(surname)"
    }
    
    @IBAction func saveButton(_ sender: Any) {
        delegate?.showNameSurname(name: name, surname: surname)
    }
}
