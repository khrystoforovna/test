//
//  SecondViewController.swift
//  test
//
//  Created by Andrey Maksymov on 03.10.2021.
//

import UIKit

class SecondViewController: UIViewController, ButtonDelegate {
    var delegate: ButtonDelegate?
    
    @IBOutlet weak var surnametextField: UITextField!
    
    var nameString = ""
    private var zdarov = ""
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toThird" {
            let svc = segue.destination as! ThirdViewController
            svc.surname = surnametextField.text!
            svc.name = nameString
            svc.delegate = self
        }
    }
    
    func showNameSurname(name: String, surname: String) {
        delegate?.showNameSurname(name: name, surname: surname)
    }
}
