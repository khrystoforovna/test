//
//  GestureViewController.swift
//  test
//
//  Created by Andrey Maksymov on 05.10.2021.
//

import UIKit

class GestureViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var yPosition: NSLayoutConstraint!
    @IBOutlet weak var xPosition: NSLayoutConstraint!
    @IBOutlet weak var blackView: UIView!
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = String(count)
    }
    
    @IBAction func tapGesture(_ sender: UITapGestureRecognizer) {
        count += 1
        label.text = String(count)
        yPosition.constant = CGFloat(Int.random(in: 0..<Int(view.frame.height - blackView.frame.height)))
        xPosition.constant = CGFloat(Int.random(in: 0..<Int(view.frame.width - blackView.frame.width)))
        view.layoutIfNeeded()
        
    }
    
    @IBAction func panGestureAction(_ sender: UIPanGestureRecognizer) {
        blackView.center = sender.location(in: view)
        
    }
    
}
